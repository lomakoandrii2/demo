from selenium.webdriver.common.by import By


class HomePageLocators:
    LOGIN_BUTTON_IN_HEADER = (By.CSS_SELECTOR, 'span.header-login')
    MENU_ITEMS_DESKTOP = (By.CSS_SELECTOR, ' section.ammenu-menu-wrapper.-desktop.-icons-desktop .ammenu-items')
    EXPECTED_MENU_ITEMS = ["Servers", "Storage", "Networking", "Components", "Support & Services"]


class LoginPageLocators:
    LOGIN_URL = "https://www.enterasource.com/customer/account/login/"
    EMAIL_FIELD = (By.CSS_SELECTOR, ".block.block-customer-login #email")
    LOGIN_FIELD = (By.CSS_SELECTOR, ".block.block-customer-login [name=login[password]]")
    LOGIN_BUTTON = (By.CSS_SELECTOR, ".block.block-customer-login #send2.action.login.primary")
    EMAIL_ERROR = (By.ID, "email-error")
    PASS_ERROR = (By.ID, "pass-error")
    EXPECTED_EMAIL_ERROR = "This is a required field."
    EXPECTED_PASS_ERROR = "This is a required field."


class ProdcutPageLocators:
    SPECS = (By.ID, "tab-label-additional-title")
    SPECS_TAB = (By.ID, "additional")
    REQUEST_A_QUOTE_BUTTON = (By.ID, "product-addtoquote-button-separate")
    QUOTE_COUNTER = (By.CSS_SELECTOR, ".action.showquote .counter-number")
