import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options as chrome_options


def get_chrome_options():
    options = chrome_options()
    options.add_argument("headless")  # headless or chrome
    options.add_argument("--start-maximized")
    options.add_argument("--window-size=1920,1080")
    options.binary_location = "/usr/bin/google-chrome"  # Путь к бинарному файлу Chrome в контейнере
    options.add_argument(f"--disable-dev-shm-usage")  # Добавьте этот параметр для устранения ошибки на CI/CD
    options.add_argument(f"--no-sandbox")  # И этот параметр тоже
    options.add_argument(f"--disable-extensions")
    options.add_argument(f"--disable-gpu")
    options.add_argument(f"--disable-infobars")
    options.add_argument(f"--remote-debugging-port=9222")
    options.add_argument(f"--disable-browser-side-navigation")
    options.add_argument(f"--disable-web-security")
    options.add_argument(f"--disable-site-isolation-trials")
    options.add_argument(f"--user-data-dir=/tmp/user-data")
    options.add_argument(f"--v=99")
    options.add_argument(f"--single-process")
    options.add_argument(f"--data-path=/tmp/data-path")
    options.add_argument(f"--ignore-certificate-errors")
    options.add_argument(f"--homedir=/tmp")
    options.add_argument(f"--disk-cache-dir=/tmp/cache-dir")
    options.add_argument(f"--metrics-recording-only")
    options.add_argument(f"--disable-default-apps")
    options.add_argument(f"--no-first-run")
    options.add_argument(f"--disable-background-networking")
    options.add_argument(f"--safebrowsing-disable-auto-update")
    options.add_argument(f"--safebrowsing-disable-download-protection")

    return options


@pytest.fixture(scope="function")
def driver(request):
    options = get_chrome_options()
    driver = webdriver.Chrome(options=options)
    request.cls.driver = driver
    yield driver
    driver.quit()
