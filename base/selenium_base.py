from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


class SeleniumBase:
    def __init__(self, driver, url):
        self.driver = driver
        self.url = url
        self.wait = WebDriverWait(self.driver, 10, 0.3)

    def open(self):
        self.driver.get(self.url)

    def is_present(self, how, what) -> WebElement:
        return self.wait.until(EC.presence_of_element_located((how, what)))

    def is_visible(self, how, what) -> WebElement:
        return self.wait.until(EC.visibility_of_element_located((how, what)))

    def are_present(self, how, what):
        return self.wait.until(EC.presence_of_all_elements_located((how, what)))

    def are_visible(self, how, what):
        return self.wait.until(EC.presence_of_all_elements_located((how, what)))

    @staticmethod
    def lists_comparison(expected_lst, my_lst):
        actual_items = []
        for i in range(len(my_lst)):
            actual_items.append(my_lst[i].text)
        actual_items = actual_items[0].split('\n')
        assert actual_items == expected_lst
