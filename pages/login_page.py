from base.selenium_base import SeleniumBase
from base.locators import LoginPageLocators


class LoginPage(SeleniumBase):

    def should_be_login_url(self):
        assert self.driver.current_url in LoginPageLocators.LOGIN_URL

    def put_text_email_field(self, test_text):
        email_field = self.is_visible(*LoginPageLocators.EMAIL_FIELD)
        email_field.send_keys(test_text)

    def put_text_login_field(self, test_text):
        login_field = self.is_visible(*LoginPageLocators.LOGIN_FIELD)
        login_field.send_keys(test_text)

    def click_on_login_button(self):
        login_button = self.is_visible(*LoginPageLocators.LOGIN_BUTTON)
        login_button.click()

    def email_error_is_correct(self):
        email_error = self.is_visible(*LoginPageLocators.EMAIL_ERROR)
        assert email_error.text == LoginPageLocators.EXPECTED_EMAIL_ERROR

    def pass_error_is_correct(self):
        pass_error = self.is_visible(*LoginPageLocators.PASS_ERROR)
        assert pass_error.text == LoginPageLocators.EXPECTED_PASS_ERROR

